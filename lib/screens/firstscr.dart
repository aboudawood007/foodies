import 'package:flutter/material.dart';
import 'package:foodies/screens/filters.dart';
import 'package:foodies/screens/meals.dart';
import 'filters.dart';
import '../classes/class1.dart';
import '../classes/class2.dart';
import 'meals.dart';
List<Item> foods = [
  Item('Italian', Colors.purple),
  Item('Quick & Easy', Colors.red.shade400),
  Item('Hamburgers', Colors.amber),
  Item('German', Colors.yellowAccent),
  Item('Light & Lovely', Colors.blue),
  Item('Exotic', Colors.green),
  Item('Breakfast', Colors.blueAccent),
  Item('Asian', Colors.greenAccent),
  Item('French', Colors.pink.shade600),
  Item('summer', Colors.teal),
];

class Foodies extends StatelessWidget {
  const Foodies({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
      title: const Text(
        'Categories',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),),
        drawer: SafeArea(
            child: Drawer(
          child: ListView(
            children: [
              Container(
                  color: Colors.amber,
                  child: ListTile(
                    title: const Text(
                      'Cooking up!',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  )),
              InkWell(onTap: (){Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const Meals())
              );},
                child: ListTile(
                  leading: const Icon(Icons.dining_rounded),
                  title: const Text(
                    'Meals',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              InkWell(onTap: (){Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const Filters())
              );},
                child: ListTile(
                  leading: const Icon(Icons.settings),
                  title: const Text(
                    'Filters',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ],
          ),
        )),
        body: Padding(
          padding: const EdgeInsets.all(15.0),
          child: InkWell(onTap: (){Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const Meals())
          );},
            child: GridView.builder(
                itemCount: foods.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 15,
                    mainAxisSpacing: 15,
                    childAspectRatio: 1.3),
                itemBuilder: (context, i) {
                  return WidgetItem(
                    title: foods[i].name,
                    colour: foods[i].color,
                  );
                }),
          ),
        ));
  }
}
