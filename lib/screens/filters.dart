import 'package:flutter/material.dart';

class Filters extends StatelessWidget {
  const Filters({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            'My Filters',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        body: ListView(
          children: [SizedBox(height:10),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              const Text(
                'Adjust your meal selection',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
              )
            ]),
            ListTile(
              trailing: Icon(Icons.toggle_off_outlined),
              title: Text(
                'Gluten',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: const Text('only includes gluten meals'),
            ),
            ListTile(
              trailing: const Icon(Icons.toggle_on_outlined),
              title: Text(
                'Lactose',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: const Text('only includes lactose meals'),
            ),
            ListTile(
              trailing:const Icon(Icons.toggle_off_outlined),
              title: const Text(
                'Vegeterian',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: const Text('only includes vegeterian meals'),
            ),
            ListTile(
              trailing: const Icon(Icons.toggle_on_outlined),
              title: const Text(
                'Vegan',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: const Text('only includes vegan meals'),
            )
          ],
        ));
  }
}
