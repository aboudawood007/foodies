import 'package:flutter/material.dart';

import '../screens/firstscr.dart';

class WidgetItem extends StatelessWidget {
  String? title;
  Color? colour;

  WidgetItem({this.colour, this.title});

  @override
  Widget build(BuildContext context) {
    return Container(
            decoration: BoxDecoration(
              color: colour,
              boxShadow: [BoxShadow(color: Colors.grey, spreadRadius: 1)],
              borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
                title: Text(
              '$title',
              style: TextStyle(fontWeight: FontWeight.bold),
            )),
          );

  }
}
